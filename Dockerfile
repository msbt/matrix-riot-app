FROM cloudron/base:1.0.0@sha256:147a648a068a2e746644746bbfb42eb7a50d682437cead3c67c933c546357617

MAINTAINER Authors name <support@cloudron.io>

RUN mkdir -p /app/code/nginx

WORKDIR /app/code

ENV MATRIXVERSION=v1.12.3

EXPOSE 8000
EXPOSE 3478/udp
# addons wip
# EXPOSE 9000

# nginx
RUN rm /etc/nginx/sites-enabled/*
ADD nginx_matrix.conf /etc/nginx/sites-enabled/
RUN rm -rf /var/lib/nginx && ln -sf /app/data/nginx /var/lib/nginx
RUN rm -rf /var/log/nginx && ln -sf /run/nginx_log /var/log/nginx

# TURN
RUN apt update && apt-get install -y coturn
RUN sed -e 's,#TURNSERVER_ENABLED=1,TURNSERVER_ENABLED=1,' -i /etc/default/coturn && \
    rm /etc/turnserver.conf && \
    ln -s /app/data/turnserver.conf /etc/turnserver.conf

# Synapse
# a local copy is needed because of the mail templates, else the pip installation would suffice
RUN git clone https://github.com/matrix-org/synapse.git
RUN cd /app/code/synapse && git pull origin master

RUN apt-get install -y build-essential python3-dev libffi-dev \
                     python3-pip python-setuptools sqlite3 libxml2-dev \
                     libssl-dev python-virtualenv libjpeg-dev libxslt1-dev libldap2-dev libsasl2-dev && \
    rm -rf /var/cache/apt /var/lib/apt/lists

RUN pip3 install --upgrade pip
RUN pip3 install --upgrade setuptools
RUN pip3 install psycopg2-binary python-ldap matrix-synapse-ldap3 lxml netaddr twisted jinja2 bleach bcrypt
RUN pip3 install https://github.com/matrix-org/synapse/archive/${MATRIXVERSION}.tar.gz

# install addons
#RUN cd /app/code && git clone http://github.com/turt2live/matrix-appservice-webhooks
#RUN cd /app/code/matrix-appservice-webhooks && npm install
#RUN mv /app/code/matrix-appservice-webhooks/db /app/code/matrix-appservice-webhooks/db_orig && \
    #dir for sqlite db
#    ln -sf /app/data/appservice/db /app/code/matrix-appservice-webhooks/db && \
    # config
#    ln -sf /app/data/appservice/config/config.yaml /app/code/matrix-appservice-webhooks/config/config.yaml && \
    # hooks
#    ln -sf /app/data/appservice/appservice-registration-webhooks.yaml /app/code/matrix-appservice-webhooks/appservice-registration-webhooks.yaml


# todo:
# to create the config, run the following from /app/code/matrix-appservice-webhooks
# node index.js -r -u "http://localhost:9000" -c config/config.yaml
# add this to homeserver.yml and restart:
# app_service_config_files: ["appservice-registration-webhooks.yaml"]
# to start the appservice use
# node index.js -p 9000 -c config/config.yaml -f appservice-registration-webhooks.yaml

RUN chown -R www-data.www-data /app/code

ADD start_matrix.sh /app/

CMD [ "/app/start_matrix.sh" ]
