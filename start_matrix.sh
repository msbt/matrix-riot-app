#!/bin/bash

set -eux

if [[ ! -f /app/data/synapse/homeserver.yaml ]]; then
    echo "=> Detected first run"

    # create dirs (for whatever reason the nginx dir couldn't get created by  nginx)
    mkdir -p /app/data/synapse \
             /app/data/nginx/fastcgi \
             /run/nginx_log \
             /run/synapse \
             /run/turn_log \
	     /app/data/appservice/config /app/data/appservice/db

    # copy matrix-appservice-webhooks config
#    cp /app/code/matrix-appservice-webhooks/config/sample.yaml /app/data/appservice/config/config.yaml
#    ln -sf /app/data/appservice/appservice-registration-webhooks.yaml /app/data/synapse/appservice-registration-webhooks.yaml

    # create certs
    openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out /app/data/synapse/${CLOUDRON_APP_DOMAIN}.tls.crt -keyout /app/data/synapse/${CLOUDRON_APP_DOMAIN}.tls.key -subj "/C=DE/ST=Germany/L=Germany/O=Example/OU=Example/CN=${CLOUDRON_APP_DOMAIN}"

    # fix permissions
    chown -R www-data.www-data /run/synapse \
                               /run/turn_log \
                               /app/data

    # copy turn config
    cp /usr/share/coturn/examples/etc/turnserver.conf /app/data/turnserver.conf

    # copy email templates for matrix
    cp -r /app/code/synapse/synapse/res/templates /app/data/templates

    # set default TURN listening port
    if [ -z ${TURN_TLS_PORT+x} ]; then TURN_TLS_PORT="3478"; else echo "TURN_TLS_PORT is set to '$TURN_TLS_PORT'"; fi

    cd /app/data/synapse
    python3 -m synapse.app.homeserver \
	    --server-name ${CLOUDRON_APP_DOMAIN} \
	    --config-path homeserver.yaml \
	    --report-stats=no \
	    --generate-config

    # synapse config
    sed -i "s/server_name:.*/server_name: ${CLOUDRON_APP_DOMAIN}/" homeserver.yaml
    sed -i "s/#public_baseurl:.*/public_baseurl: https:\/\/${CLOUDRON_APP_DOMAIN}/" homeserver.yaml
    sed -i "s/web_client:.*/web_client: False/" homeserver.yaml
    sed -i "s/bind_addresses: \['::1'.*/bind_addresses: \['0.0.0.0'\]/" homeserver.yaml
    sed -i "s/sqlite3/psycopg2/" homeserver.yaml
    sed -i "s/    database: .*/    user: ${CLOUDRON_POSTGRESQL_USERNAME}\n    password: ${CLOUDRON_POSTGRESQL_PASSWORD}\n    database: ${CLOUDRON_POSTGRESQL_DATABASE}\n    host: ${CLOUDRON_POSTGRESQL_HOST}\n    cp_min: 5\n    cp_max: 10/" homeserver.yaml
    sed -i "s/#enable_registration:.*/enable_registration: True/" homeserver.yaml
    sed -i "s/#password_providers:/password_providers:/" homeserver.yaml
    sed -i 's/#    - module: "ldap_auth_provider.LdapAuthProvider"/    - module: "ldap_auth_provider.LdapAuthProvider"/' homeserver.yaml
    sed -i 's/#      config:/      config:/' homeserver.yaml
    sed -i 's/#        enabled: true/        enabled: true/' homeserver.yaml
    sed -i "s,#        uri: .*,        uri: \"${CLOUDRON_LDAP_URL}\"," homeserver.yaml
    sed -i 's/#        start_tls: true/        start_tls: false/' homeserver.yaml
    sed -i 's/#        base: "ou=users,dc=example,dc=com"/        base: "ou=users,dc=cloudron"/' homeserver.yaml
    sed -i 's/#        attributes:/        attributes:/' homeserver.yaml
    sed -i 's/#           uid: "cn"/          uid: "username"/' homeserver.yaml
    sed -i 's/#           mail: "email"/          mail: "mail"/' homeserver.yaml
    sed -i 's/#           name: "givenName"/          name: "username"/' homeserver.yaml
    sed -i 's/#max_upload_size:.*/max_upload_size: "500M"/' homeserver.yaml
    sed -i 's/#auto_join_rooms:/auto_join_rooms:/' homeserver.yaml
    sed -i 's/#  - "#example:example.com"/  - "#example:example.com"/' homeserver.yaml
    sed -i "s/example:example.com/discuss:${CLOUDRON_APP_DOMAIN}/" homeserver.yaml
    sed -i "s/turn_allow_guests:.*/turn_allow_guests: False/" homeserver.yaml
    sed -i "s/enable_group_creation:.*/enable_group_creation: True/" homeserver.yaml
    sed -i "s/enable_group_creation:.*/enable_group_creation: True/" homeserver.yaml
    sed -i "s/#url_preview_enabled:.*/url_preview_enabled: true/" homeserver.yaml
    sed -i "s/#url_preview_ip_range_blacklist.*/url_preview_ip_range_blacklist:\n  - '127.0.0.0\/8'\n  - '10.0.0.0\/8'/" homeserver.yaml

    sed -i "s/## Server ##/## Server ##\n\nno_tls: True/" homeserver.yaml

    # also enable the tls port, not quite sure why this is needed, but without it, no integratiosn are possible
    #sed -i "s/#- port: 8448/- port: 8448/" homeserver.yaml
    #sed -i "s/#  type: http/  type: http/" homeserver.yaml
    #sed -i "s/#  tls: true/  tls: true/" homeserver.yaml
    #sed -i "s/#  resources:/  resources:/" homeserver.yaml
    #sed -i "s/#    - names: \[client, federation\]/    - names: \[client, federation\]/" homeserver.yaml
    #sed -i "s/#tls_certificate_path:/tls_certificate_path:/" homeserver.yaml
    #sed -i "s/#tls_private_key_path:/tls_private_key_path:/" homeserver.yaml


    # get synapse log name and set path
    LOG_CONFIG=$(ls *.log.config)
    sed -i "s/log_file: .*/log_file: \"\/run\/synapse\/homeserver.log\"/" homeserver.yaml
    sed -i "s/    filename:.*/    filename: \/run\/synapse\/homeserver.log/" ${LOG_CONFIG}


    # coturn
    TURNPWD=$(pwgen -s 64 1)
    sed -i "s/#tls-listening-port=5349/tls-listening-port=3478/" /app/data/turnserver.conf
    sed -i "s/#realm=mycompany.org/realm=${CLOUDRON_APP_DOMAIN}/" /app/data/turnserver.conf
    sed -i "s/#lt-cred-mech/lt-cred-mech/" /app/data/turnserver.conf
    sed -i "s/#use-auth-secret/use-auth-secret/" /app/data/turnserver.conf
    sed -i "s/#lt-cred-mech/lt-cred-mech/" /app/data/turnserver.conf
    sed -i "s/#static-auth-secret=.*/static-auth-secret=${TURNPWD}/" /app/data/turnserver.conf
    sed -i "s/#turn_uris: .*/turn_uris: [\"turn:${CLOUDRON_APP_DOMAIN}:${TURN_TLS_PORT}?transport=udp\", \"turn:${CLOUDRON_APP_DOMAIN}:${TURN_TLS_PORT}?transport=tcp\"]/" homeserver.yaml
    sed -i "s/#turn_shared_secret: .*/turn_shared_secret: \"${TURNPWD}\"/" homeserver.yaml
    sed -i "s/#cipher-list=.*/cipher-list=\"HIGH\"/" /app/data/turnserver.conf
    sed -i "s/#log-file=.*/log-file=\/run\/turn_log\/turn.log/" /app/data/turnserver.conf


    # get cert names from synapse
    TLS_CRT=$(ls *.tls.crt)
    TLS_KEY=$(ls *.tls.key)
    #TLS_DH=$(ls *.tls.dh)
    sed -i "s,#cert=.*,cert=/app/data/synapse/${TLS_CRT}," /app/data/turnserver.conf
    sed -i "s,#pkey=.*,pkey=/app/data/synapse/${TLS_KEY}," /app/data/turnserver.conf
    #sed -i "s,#dh-file=.*,dh-file=/app/data/synapse/${TLS_DH}," /app/data/turnserver.conf

    # set up email
    sed -i 's/#email:/email:/' homeserver.yaml
    sed -i 's/#   enable_notifs: false/   enable_notifs: true/' homeserver.yaml
    sed -i 's/#   smtp_host: \"localhost\"/   smtp_host: \"${CLOUDRON_MAIL_SMTP_SERVER}\"/' homeserver.yaml
    sed -i 's/#   smtp_port: .*/   smtp_port: ${CLOUDRON_MAIL_SMTP_PORT}/' homeserver.yaml
    sed -i 's/#   smtp_user: \"exampleusername\"/   smtp_user: \"${CLOUDRON_MAIL_SMTP_USERNAME}\"/' homeserver.yaml
    sed -i 's/#   smtp_pass: \"examplepassword\"/   smtp_pass: \"${CLOUDRON_MAIL_SMTP_PASSWORD}\"/' homeserver.yaml
    sed -i 's/#   require_transport_security: False/   require_transport_security: false/' homeserver.yaml
    sed -i "s/#   notif_from: .*/   notif_from: \"Your Friendly Matrix Home Server <${CLOUDRON_MAIL_FROM}>\"/" homeserver.yaml
    sed -i 's/#   app_name: Matrix/   app_name: Matrix/' homeserver.yaml
    sed -i 's/#   notif_template_html: .*/   notif_template_html: notif_mail.html/' homeserver.yaml
    sed -i 's/#   notif_template_text: .*/   notif_template_text: notif_mail.txt/' homeserver.yaml
    sed -i 's/#   notif_for_new_users: True/   notif_for_new_users: True/' homeserver.yaml

    sed -i "s,#tls_fingerprints:.*,tls_fingerprints: [{sha256: \"${TLS_FINGERPRINT}\"}]," homeserver.yaml


fi

# logs
mkdir -p /run/nginx_log /run/synapse /run/turn_log

# fix permissions
chown -R www-data.www-data /run/synapse /run/turn_log /app/data
cd /app/data/synapse

# check if TURN port has changed and update it
if [ -z ${TURN_TLS_PORT+x} ]; then TURN_TLS_PORT="3478"; fi
sed -i "s/turn_uris: .*/turn_uris: [\"turn:${CLOUDRON_APP_DOMAIN}:${TURN_TLS_PORT}?transport=udp\", \"turn:${CLOUDRON_APP_DOMAIN}:${TURN_TLS_PORT}?transport=tcp\"]/" homeserver.yaml
sed -i "s/tls-listening-port=.*/tls-listening-port=${TURN_TLS_PORT}/" /app/data/turnserver.conf

# check if certificate changed and update fingerprint
TLS_FINGERPRINT=$(openssl s_client -connect ${CLOUDRON_APP_DOMAIN}:${TURN_TLS_PORT} < /dev/null 2> /dev/null | openssl x509 -outform DER | openssl sha256 -binary | base64 | tr -d '=')
sed -i "s,^tls_fingerprints:.*,tls_fingerprints: [{sha256: \"${TLS_FINGERPRINT}\"}]," homeserver.yaml

gosu www-data turnserver -c /app/data/turnserver.conf --daemon -v

# update user and pass in case they changed
sed -i "s/ user: .*/ user: ${CLOUDRON_POSTGRESQL_USERNAME}/" homeserver.yaml
sed -i "s/ password: .*/ password: ${CLOUDRON_POSTGRESQL_PASSWORD}/" homeserver.yaml
sed -i "s/ database: .*/ database: ${CLOUDRON_POSTGRESQL_DATABASE}/" homeserver.yaml
sed -i "s,         uri: .*,         uri: \"${CLOUDRON_LDAP_URL}\"," homeserver.yaml

#update email settings in case they changed
sed -i "s/   smtp_host:.*/   smtp_host: \"${CLOUDRON_MAIL_SMTP_SERVER}\"/" homeserver.yaml
sed -i "s/   smtp_port:.*/   smtp_port: ${CLOUDRON_MAIL_SMTP_PORT}/" homeserver.yaml
sed -i "s/   smtp_user:.*/   smtp_user: \"${CLOUDRON_MAIL_SMTP_USERNAME}\"/" homeserver.yaml
sed -i "s/   smtp_pass:.*/   smtp_pass: \"${CLOUDRON_MAIL_SMTP_PASSWORD}\"/" homeserver.yaml

gosu www-data python3 -m synapse.app.homeserver --config-path homeserver.yaml &> /dev/null &

exec /usr/sbin/nginx -g 'daemon off;'
